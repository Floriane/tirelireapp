﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /****************************************************************************************************/
    /* MontantTirelire : ecran qui permet dafficher la somme du contenu d'une tirelire                  */
    /****************************************************************************************************/
    public partial class MontantTirelire : ContentPage
    {
        private Tirelire tirelire;

		public MontantTirelire (Tirelire tirelire)
		{
			InitializeComponent ();
            this.tirelire = tirelire;
            var uri = new Uri(string.Format("http://192.168.1.47/API_CA/api/moneyBox/readMoneyJwt"));   // tentative de login 
            var json = JsonConvert.SerializeObject(new RequeteMontant(Preferences.Get("jwt", "-1"), tirelire.nomTirelire));
            var contentLogin = new StringContent(json, Encoding.UTF8, "application/json");
            HttpClient clientLogin = new HttpClient();
            clientLogin.PostAsync(uri, contentLogin).ContinueWith((postTask) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    afficherMontantAsync(postTask);
                });
            });
        }

        private async void afficherMontantAsync(Task<HttpResponseMessage> reponse)
        {
            var retour = reponse.Result;
            var body = await retour.Content.ReadAsStringAsync();
            var bodyObject = JsonConvert.DeserializeObject<ReponseMontantTirelire>(body);

            montantTexte.Text = "Le montant est " + bodyObject.faireLaSomme().ToString() + " euros.";
        }
	}
}