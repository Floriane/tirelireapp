﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /********************************************************************************/
    /* HomeTirelire : ecran qui permet à l'utilisateur d'acceder aux differentes     */
    /* actions possibles sur une tirelire en particulier                            */
    /********************************************************************************/
	public partial class HomeTirelire : ContentPage
    {
        private Tirelire tirelire ;

        public HomeTirelire(Tirelire t)
        {
            InitializeComponent();
            tirelire = t;

            titre.Text = tirelire.nomTirelire;

            List<string> listeNomActions = new List<string>();
            if (tirelire.role != Role.Indefini)
            {
                listeNomActions.Add("Consulter solde du compte");
                listeNomActions.Add("Consulter solde de la tirelire");
                if (tirelire.role == Role.Ami || tirelire.role == Role.Parent || tirelire.role == Role.Proprietaire)
                {
                    listeNomActions.Add("Virement");
                    listeNomActions.Add("Envoi message");
                }
                if (tirelire.role == Role.Parent || tirelire.role == Role.Proprietaire)
                {
                    listeNomActions.Add("Ajouter utilisateur");
                }
                if (tirelire.role == Role.Proprietaire)
                {
                    listeNomActions.Add("Changer adresse mail du propriétaire");
                }
            }

            listeActions.ItemsSource = listeNomActions;

            listeActions.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) =>
            {
                string actionNom = e.SelectedItem.ToString();
                switch (actionNom)
                {
                    case "Ajouter utilisateur":
                        await Navigation.PushAsync(new AjouterUtilisateur(tirelire));
                        break;
                    case "Consulter solde de la tirelire":
                        await Navigation.PushAsync(new MontantTirelire(tirelire));
                        break;
                    case "Changer adresse mail du propriétaire":
                        await Navigation.PushAsync(new ChangerMail(tirelire));
                        break;
                    default:
                        break;
                }       
            };
        }

	}
}