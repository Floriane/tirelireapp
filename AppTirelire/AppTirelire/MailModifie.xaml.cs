﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /********************************************************************************************************************/
    /* MailModifie : ecran qui confirme le changement de mail, permet de retourner a la page principale de la tirelire  */
    /*******************************************************************************************************************/
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MailModifie : ContentPage
	{
        private Tirelire tirelire; 

		public MailModifie (Tirelire tirelire)
		{
			InitializeComponent ();
            this.tirelire = tirelire;
            info.Text = "Votre mail a été modifié";
		}

        async void ActionRetour(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HomeTirelire(tirelire));
        }
    }
}