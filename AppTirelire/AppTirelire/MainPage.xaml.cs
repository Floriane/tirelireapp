﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http;
using Plugin.DeviceInfo;

namespace AppTirelire
{
    /********************************************************************/
    /*Main page : page qui permet a l'utilisateur de s'identifier lors  */
    /* de la premiere connexion.                                        */
    /********************************************************************/
    public partial class MainPage : ContentPage
    {
        private string code;                                            // code saisi par l'utilisateur
        private string email;                                           // mail saisi par l'utilisateur
        private HttpClient client;
        private HttpClient clientLogin;

        public MainPage()
        {
            InitializeComponent();
            client = new HttpClient();
            clientLogin = new HttpClient();
        }

        void EntryMailTexteModifie(object sender, TextChangedEventArgs e)
        {
            email = e.NewTextValue;                                     // mise a jour de la valeur du mail saisie
        }

        void EntryCodeTexteModifie(object sender, TextChangedEventArgs e)
        {
            code = e.NewTextValue;                                      // mise ajour de la valeur du code saisie
        }

        async void ActionValider(object sender, EventArgs args)
        {
            messsageErreur.Text = "Connexion en cours";

            string numeroDeSerie = CrossDeviceInfo.Current.Id;                                                           // recuperation du numero de serie du telephone   
            var uri = new Uri(string.Format("http://192.168.1.47/API_CA/api/moneyBoxesUser/confirmUser.php"));           // adresse de l'api pour creer un compte
            var json = JsonConvert.SerializeObject(new Confirmation(email,code, numeroDeSerie));                         // creation du corps de la requete
            var content = new StringContent(json, Encoding.UTF8, "application/json");                           
            HttpResponseMessage response = await client.PostAsync(uri, content);                                        // requete HTTP

            if (response.IsSuccessStatusCode)                                                                           // si le compte a pu etre ajoute
            {
                Preferences.Set("email", email);                                                                        // mail qui est le login, stocke pour ne plus etre saisi par l'utilisateur
                await Navigation.PushAsync(new Home());
            }
            else
            {
                messsageErreur.Text = "Le mail et/ou le code sont incorrectes.";                                     // generer un message d'erreur
            }        
        }
    }
}
