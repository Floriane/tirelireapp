﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AppTirelire
{
    /**********************************************************/
    /* Tirelire : classe qui represente une tirelire          */
    /**********************************************************/
    public class Tirelire
    {
        public String nomTirelire  { get; set; }               // Nom de la tirelire, permet de la reperer dans une liste
        public String nomEnfant    { get; set; }               // A voir si on a le nom de l'enfant
        public Role role           { get; set; }               // Role du compte connecte pour la tirelire



        /********************************************************************/
        /*Tirelire           Constructeur d'une tirelire                    */
        /*                                                                  */
        /*Entree : pnomTirelire (String) nom de la nouvelle tirelire        */
        /*                                                                  */
        /*Sortie : void                                                     */
        /********************************************************************/
        public Tirelire(String pnomTirelire)
        {
            nomTirelire = pnomTirelire;                       // Une tirelire a au minimum un nom
            nomEnfant = "";                                   // A  voir si on le nom de l'enfant
            role = Role.Indefini;                            // Le role n'a pas encore ete demande au serveur
        }


        /********************************************************************/
        /*ToString           surcharge de la methode ToString, necessaire   */
        /*                   pour l'affichage dans les listeView            */
        /*                                                                  */
        /*Entree : void                                                     */
        /*                                                                  */
        /*Sortie : String : nom de la tirelire                              */
        /********************************************************************/
        public override string ToString()
        {
            return nomTirelire;
        }

    }
}
