﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /*************************************************************************************************************/
    /* ChangerMail : ecran qui permet de modifier le mail de l'utilisateur, s'il le proprietaire de la tirelire */
    /************************************************************************************************************/
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChangerMail : ContentPage
	{
        private string email;
        private string emailConfirme;
        private Tirelire tirelire;

        public ChangerMail (Tirelire tirelire)
		{
			InitializeComponent ();
            this.tirelire = tirelire;
		}

        void EntryMailTexteModifie(object sender, TextChangedEventArgs e)
        {
            email = e.NewTextValue;                                     // mise a jour de la valeur du mail saisie
        }

        void EntryConfirmationMailTexteModifie(object sender, TextChangedEventArgs e)
        {
            emailConfirme = e.NewTextValue;                                     
        }

        async void ActionValider(object sender, EventArgs args)
        {
            if(email != "" && email == emailConfirme)
            {
                HttpClient client = new HttpClient();
                var uri = new Uri(string.Format("http://192.168.1.47/API_CA/api/user/updateEmail.php"));           // adresse de l'api pour creer un compte
                var json = JsonConvert.SerializeObject(new NouveauMail(email, Preferences.Get("jwt","-1"), tirelire.nomTirelire));                         // creation du corps de la requete
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);                                        // requete HTTP

                if (response.IsSuccessStatusCode)                                                                           // si le compte a pu etre ajoute
                {
                    Preferences.Set("email", email);                                                                        // mail qui est le login, stocke pour ne plus etre saisi par l'utilisateur
                    await Navigation.PushAsync(new MailModifie(tirelire));
                }
                else
                {
                    messsageErreur.Text = "Erreur de connexion.";                                     // generer un message d'erreur
                }

            }
            else
            {
                messsageErreur.Text = "Veuillez saisir deux mails identiques.";
            }
        }
    }
}