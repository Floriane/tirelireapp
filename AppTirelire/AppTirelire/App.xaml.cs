﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AppTirelire
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var mail = Preferences.Get("email", "-1");
            if( mail == "-1")                                     // si l'utilisateur ne s'est jamais connecte
            {
                MainPage = new NavigationPage(new MainPage());  // on va a la page de premiere connexion (saisie email et code)
            }
            else
            {
               MainPage = new NavigationPage(new Home());      // si l'utilisateur s'est deja connecte, on va directement a la page home avec le choix des tirelires
            }
           
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
