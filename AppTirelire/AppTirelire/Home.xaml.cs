﻿using Newtonsoft.Json;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /********************************************************************************************/
    /* Home : ecran qui contient un acces a toutes les tirelires associees au compte connecte    */
    /********************************************************************************************/
	public partial class Home : ContentPage
	{
		public Home ()
		{
			InitializeComponent ();

            informations.Text = "Connexion en cours";
            // Connexion pour récuperation du jeton JWT
            var uriLogin = new Uri(string.Format("http://192.168.1.47/api_ca/api/authentification/login_mobil.php"));   // tentative de login 
            var jsonLogin = JsonConvert.SerializeObject(new Authentification(Preferences.Get("email", "-1"), CrossDeviceInfo.Current.Id));
            var contentLogin = new StringContent(jsonLogin, Encoding.UTF8, "application/json");
            HttpClient clientLogin = new HttpClient();
            clientLogin.PostAsync(uriLogin, contentLogin).ContinueWith((postTask) =>
             {
                 recupererTirelireAsync(postTask);
             });
        }

        private async void recupererTirelireAsync(Task<HttpResponseMessage> postTask)
        {
            var reponse = postTask.Result; 

            if (reponse != null)
            {

                if (reponse.IsSuccessStatusCode)
                {
                    informations.Text = "Recherche de vos tirelires";
                    var body = await reponse.Content.ReadAsStringAsync();
                    var retour = JsonConvert.DeserializeObject<ReponseLogin>(body);
                    Preferences.Set("jwt", retour.jwt);


                    // recuperation des tirelires
                    var uriTirlires = new Uri(string.Format("http://192.168.1.47/API_CA/api/moneyBoxesUser/readMoneybox.php"));
                    var jsonTirelires = JsonConvert.SerializeObject(new ChercherTirelires(retour.jwt));
                    var contentTirelires = new StringContent(jsonTirelires, Encoding.UTF8, "application/json");
                    HttpClient clientTirelires = new HttpClient();
                    var retourTirelires = await clientTirelires.PostAsync(uriTirlires, contentTirelires);

                    if(retourTirelires.IsSuccessStatusCode)
                    {
                        // afficher les tirelires
                        var bodyTirelires = await retourTirelires.Content.ReadAsStringAsync();
                        var retourTireliresObjet = JsonConvert.DeserializeObject<ReponseTirelires>(bodyTirelires);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            afficherTirelire(retourTireliresObjet);
                        });                       
                    }
                    else
                    {;
                        informations.Text = "Echec de la recherche de tirelire.";
                    }
     
                }
                else
                {
                    informations.Text = "Echec de la connexion";
                }
            }
        }

        private void afficherTirelire(ReponseTirelires reponse)
        {
            informations.Text = "Liste de vos tirelires";

            ListeTirelire.clear();

            foreach (MoneyBox moneyBox in reponse.records)
            {
                Tirelire nouvelleTirelire = new Tirelire(moneyBox.id_money_box);
                ListeTirelire.ajouterTirelire(nouvelleTirelire);
            }
       
            listeTirelire.ItemsSource = ListeTirelire.getTirelires();                               // afficher les tirelires comme elements de la liste
                                   
            listeTirelire.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) =>
            {
                string nomTirelire = e.SelectedItem.ToString();
                int indexe = ListeTirelire.chercherTirelire(nomTirelire);

                if (indexe != -1)
                {
                    Tirelire tirelireSelectionnee = ListeTirelire.getTirelires()[indexe];
                    if (tirelireSelectionnee.role == Role.Indefini)
                    {
                        HttpClient client = new HttpClient();
                        var uri = new Uri(string.Format("http://192.168.1.47/API_CA/api/moneyBoxesUser/readRole.php"));
                        var json = JsonConvert.SerializeObject(new RolePourTirelire(tirelireSelectionnee.ToString(), Preferences.Get("jwt", "-1")));
                        var content = new StringContent(json, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = await client.PostAsync(uri, content);

                        if (response.IsSuccessStatusCode)
                        {
                            var bodyRoleTirelire = await response.Content.ReadAsStringAsync();
                            var retourRoleTirelire = JsonConvert.DeserializeObject<ReponseRoleTirelire>(bodyRoleTirelire);
                            ListeTirelire.setRoleTirelireIndex(indexe, retourRoleTirelire.role);
                            tirelireSelectionnee.role = ListeTirelire.getTirelires()[indexe].role;
                        }
                    }
                    await Navigation.PushAsync(new HomeTirelire(tirelireSelectionnee));

                }

            };

        }

        async void ActionAjouterTirelire(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new AjouterTirelire());                                      // on accede a la page pour ajouter une nouvelle tirelire a partir d'un code
        }
	}
}