﻿using Newtonsoft.Json;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /****************************************************************************************************/
    /* AjouterTirelire : page qui permet d'ajouter une tirelire a partir d'un code envoye par mail      */
    /****************************************************************************************************/
	public partial class AjouterTirelire : ContentPage
	{
        private string code;                                                                            // code saisi par l'utilisateur

        public AjouterTirelire ()
		{
			InitializeComponent ();
		}

        void EntryCodeTexteModifie(object sender, TextChangedEventArgs e)
        {
            code = e.NewTextValue;                                                                      // mise ajour de la valeur du code saisie
        }

        async void ActionValider(object sender, EventArgs args)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format("http://192.168.1.47/API_CA/api/moneyBoxesUser/confirmUser.php"));                                                   // adresse de l'api pour creer un compte
            var json = JsonConvert.SerializeObject(new Confirmation(Preferences.Get("email", "-1"), code, CrossDeviceInfo.Current.Id));                         // creation du corps de la requete
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, content);                                                                                // requete HTTP

            if (response.IsSuccessStatusCode)
            {
                var body = await response.Content.ReadAsStringAsync();
                var retour = JsonConvert.DeserializeObject<ReponseConfirmation>(body);
                await Navigation.PushAsync(new NouvelleTirelire(retour.id_money_box));                                     // si le code est bon on se rend sur la page pour annoncer l'ajout
            }
            else
            {
                messsageErreur.Text = "Le code est incorrecte ou périmé.";                              // generer un message d'erreur
            }
        }

        async void ActionRetour(object sender, EventArgs args)
        {          
            await Navigation.PushAsync(new Home());                                                     // Retour a la page de la liste des tirelires
        }

    }
}