﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /********************************************************************************************************/
    /* NouveauUtilisateur : ecran qui permet de confirmer l'ajout d'un nouvel utilisateur pour une tirelire */
    /********************************************************************************************************/
    public partial class NouveauUtilisateur : ContentPage
    {
        private Tirelire tirelire;

        public NouveauUtilisateur(string nom, Tirelire tirelire)
        {
            InitializeComponent();

            info.Text = nom + " a été ajouté";
            this.tirelire = tirelire;
        }

        async void ActionRetour(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HomeTirelire(tirelire));
        }
    }
}