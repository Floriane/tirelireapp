﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace AppTirelire
{
    /************************************************************************/
    /*ListeTirelire class static qui contient l'ensemble des tirelires      */
    /* associees au compte connecte. La mise a jour de la classe demande    */
    /* un appel au serveur                                                  */
    /************************************************************************/
    static class ListeTirelire
    {
        public static ObservableCollection<Tirelire> tirelires = new ObservableCollection<Tirelire>();         // liste des tirelires associees au compte connecte


        /********************************************************************/
        /*AjouterTirelire           permet d'ajouter une tirelire à la liste*/
        /*                                                                  */
        /*Entree : nouvelle (Tirelire) tirelire a ajoutee                   */
        /*                                                                  */
        /*Sortie : void                                                     */
        /********************************************************************/
        public static  void ajouterTirelire(Tirelire nouvelle)
        {
            tirelires.Add(nouvelle);
        }


        /********************************************************************/
        /*getTirelires           retourne la liste des tirelires            */
        /*                                                                  */
        /*Entree : void                                                     */
        /*                                                                  */
        /*Sortie : Liste<Tirelire> liste de tirelires associees au compte   */
        /********************************************************************/
        public static ObservableCollection<Tirelire> getTirelires()
        {
            return tirelires;
        }


        /********************************************************************/
        /*clear                     vide la liste des tirelires             */
        /*                                                                  */
        /*Entree : void                                                     */
        /*                                                                  */
        /*Sortie : void                                                     */
        /********************************************************************/
        public static void clear()
        {
            tirelires.Clear();
        }


        /********************************************************************/
        /*setRoleTirelireIndex         Modifie le role de la tirelire donnee*/
        /*                             en parametre                         */
        /*                                                                  */
        /*Entree : indexe (int) indice de la tirelire a modifier            */
        /*         nouveauRole (Role) role a attribue                       */
        /*                                                                  */
        /*Sortie : void                                                     */
        /********************************************************************/
        public static void setRoleTirelireIndex(int indexe, Role nouveauRole)
        {
            tirelires[indexe].role = nouveauRole;
        }

        /********************************************************************/
        /*setRoleTirelireIndex         Modifie le role de la tirelire donnee*/
        /*                             en parametre                         */
        /*                                                                  */
        /*Entree : indexe (int) indice de la tirelire a modifier            */
        /*         nouveauRole (String) role a attribue                     */
        /*                                                                  */
        /*Sortie : void                                                     */
        /********************************************************************/
        public static void setRoleTirelireIndex(int indexe, string nouveauRole)
        {
            Role tmp;
            switch (nouveauRole)
            {
                case "owner":
                    tmp = Role.Proprietaire;
                    break;
                case "parent":
                    tmp = Role.Parent;
                    break;
                case "friend":
                    tmp = Role.Ami;
                    break;
                case "children":
                    tmp = Role.Enfant;
                    break;
                default:
                    tmp = Role.Indefini;
                    break;

            }
            tirelires[indexe].role = tmp;
        }


        /********************************************************************/
        /*chercherTirelire        chercher l'indexe d'une tirelire dont on  */
        /*                        connait le nom                            */
        /*                                                                  */
        /*Entree : nom (string)nom de la tirelire a chercher                */
        /*                                                                  */
        /*Sortie : int : indexe de la tirelire cherchee, vaut -1 si la      */
        /*               la tirelire n'est pas trouvee                      */
        /********************************************************************/
        public static int chercherTirelire(string nom)
        {
            int retour = -1;
            int indexCourant = 0;

            while(indexCourant < tirelires.Count && tirelires[indexCourant].nomTirelire != nom)
            {
                indexCourant++;
            }

            if(indexCourant != tirelires.Count)
            {
                retour = indexCourant;
            }
            return retour; 
        }
    }
}
