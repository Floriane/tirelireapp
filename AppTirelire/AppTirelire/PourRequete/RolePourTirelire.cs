﻿namespace AppTirelire
{
    /**********************************************************/
    /* RolePourTirelire : classe qui correspond au format Json*/
    /* necessaire pour connaitre le role d'une tirelire       */
    /**********************************************************/
    internal class RolePourTirelire
    {
        public string id_money_box { get; set;}                 // identifiant de la tirelire concernee
        public string jwt { get; set; }                         // jeton d'auhtentification de l'utilisateur

        public RolePourTirelire(string id, string jwt)
        {
            this.id_money_box = id;
            this.jwt = jwt;
        }
    }
}