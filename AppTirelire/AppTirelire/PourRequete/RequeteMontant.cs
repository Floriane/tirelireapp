﻿namespace AppTirelire
{
    /**********************************************************/
    /* RequeteMontant : classe qui correspond au format Json  */
    /* necessaire pour connaitre le contenu d'une tirelire    */
    /**********************************************************/
    internal class RequeteMontant
    {
        public string jwt { get; set; }                         // jeton d'authentification de l'utilisateur
        public string id_money_box { get; set; }                // id de le tirelire concernee

        public RequeteMontant(string jwt, string id_money_box)
        {
            this.jwt = jwt;
            this.id_money_box = id_money_box;
        }
    }
}