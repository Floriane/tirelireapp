﻿namespace AppTirelire
{
    /**************************************************************/
    /* ReponseRoleTirelire : classe qui correspond au format Json*/
    /* de la reponse demandant le role d'une tirelire           */
    /**********************************************************/
    internal class ReponseRoleTirelire
    {
        public string role { get; set; }                        // role de l'utilisateur pour la tirelire demandee

        public ReponseRoleTirelire(string role)
        {
            this.role = role;
        }
    }
}