﻿namespace AppTirelire
{
    /*************************************************************/
    /* MoneyBoxe : classe qui correspond au format Json         */
    /* de la reponse de la recherche des tirelires, correspond  */
    /* a l'objet contenu dans la liste records                 */
    /**********************************************************/
    public class MoneyBox
    {

        public string id_money_box { get; set; }                // id de le tirelire

        public MoneyBox(string id)
        {
            id_money_box = id;
        }
    }
}