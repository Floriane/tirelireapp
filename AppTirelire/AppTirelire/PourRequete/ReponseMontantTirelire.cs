﻿using System;

namespace AppTirelire
{
    /*****************************************************************/
    /* ReponseMontantTirelire : classe qui correspond au format Json*/
    /* de la reponse donnant le contenu de la tirelire              */
    /****************************************************************/
    internal class ReponseMontantTirelire
    {
        public string coin001 { get; set; }                         // nombre de pieces de 1 centime
        public string coin002 { get; set; }                         // nombre de pieces de 2 centimes
        public string coin005 { get; set; }                         // nombre de pieces de 5 centimes
        public string coin010 { get; set; }                         // nombre de pieces de 10 centimes
        public string coin020 { get; set; }                         // nombre de pieces de 20 centimes
        public string coin050 { get; set; }                         // nombre de pieces de 50 centimes
        public string coin100 { get; set; }                         // nombre de pieces de 1 euro
        public string coin200 { get; set; }                         // nombre de pieces de 2 euros
        public string banknote5 { get; set; }                       // nombre de billets de 5 euros
        public string banknote10 { get; set; }                      // nombre de billets de 10 euros
        public string banknote20 { get; set; }                      // nombre de billets de 20 euros
        public string banknote50 { get; set; }                      // nombre de billets de 50 euros

        public ReponseMontantTirelire(string coin001, string coin002, string coin005, string coin010, string coin020, string coin050, string coin100, string coin200, string banknote5, string banknote10, string banknote20, string banknote50)
        {
            this.coin001 = coin001;
            this.coin002 = coin002;
            this.coin005 = coin005;
            this.coin010 = coin010;
            this.coin020 = coin020;
            this.coin050 = coin050;
            this.coin100 = coin100;
            this.coin200 = coin200;
            this.banknote5 = banknote5;
            this.banknote10 = banknote10;
            this.banknote20 = banknote20;
            this.banknote50 = banknote50;
        }


        public double faireLaSomme()
        {
            double somme = 0.0;
            somme = Convert.ToDouble(coin001) * 0.01 + Convert.ToDouble(coin002) * 0.02 + Convert.ToDouble(coin005) * 0.05 + Convert.ToDouble(coin010) * 0.10 + Convert.ToDouble(coin020) * 0.20 + Convert.ToDouble(coin050) * 0.50
                + Convert.ToDouble(coin100) + Convert.ToDouble(coin200) * 2 + Convert.ToDouble(banknote5) * 5 + Convert.ToDouble(banknote10) * 10 + Convert.ToDouble(banknote20) * 20 + Convert.ToDouble(banknote50) * 50;
            return somme;

        }


    }
}