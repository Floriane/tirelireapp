﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppTirelire
{
    /**************************************************************/
    /* ReponseConfirmation : classe qui correspond au format Json*/
    /* de la reponse de la confirmation d'un ajout de tirelire  */
    /***********************************************************/
    class ReponseConfirmation
    {

        public string message { get; set; }                     // message du serveur
        public string id_money_box { get; set; }                // id de la nouvelle tirelire

        public ReponseConfirmation(string message, string id)
        {
            this.message = message;
            this.id_money_box = id;
        }
    }
}
