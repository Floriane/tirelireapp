﻿namespace AppTirelire
{
    /**********************************************************/
    /* AjoutUtilisateur : classe qui correspond au format Json*/
    /* necessaire a l'ajout d'un utilisateur sur une tirelire */
    /**********************************************************/
    internal class AjoutUtilisateur
    {
        public string jwt { get; set; }                         // jeton de connexion de l'utilisateur
        public string email { get; set; }                       // mail de l'utilisateur a ajoute
        public string role { get; set; }                        // role de l'utilisateur a ajoute
        public int id_money_box { get; set; }                   // id de la tirelire concernee par l'ajout

        public AjoutUtilisateur(string jwt, string email, string roleServeur, int idTirelire)
        {
            this.jwt = jwt;
            this.email = email;
            this.role = roleServeur;
            this.id_money_box = idTirelire;
        }
    }
}