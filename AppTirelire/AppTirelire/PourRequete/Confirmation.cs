﻿namespace AppTirelire
{
    /*************************************************************/
    /* Confirmation : classe qui correspond au format Json      */
    /* necessaire a la confirmation d'un ajout de tirelire avec */
    /* un compte existant ou un nouveau compte                  */
    /************************************************************/
    internal class Confirmation
    {
        public string email { get; set; }                           // mail de l'utilisateur
        public string validation_code { get; set; }                 // code de validation envoye a son adresse mail
        public string auth_token { get; set; }                      // numero de serie du telephone

        public Confirmation(string email, string code, string mac)
        {
            this.email = email;
            this.validation_code = code;
            this.auth_token = mac;
        }
    }
}