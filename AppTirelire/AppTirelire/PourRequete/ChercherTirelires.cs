﻿namespace AppTirelire
{
    /**********************************************************/
    /* ChercherTirelire : classe qui correspond au format Json*/
    /* necessaire a la recherche de toutes les tirelires       */
    /* associees a un utilisateur.                             */ 
    /**********************************************************/
    internal class ChercherTirelires
    {
        public string jwt { get; set; }                         // jeton de connexion de l'utilisateur

        public ChercherTirelires(string jwt)
        {
            this.jwt = jwt;
        }
    }
}