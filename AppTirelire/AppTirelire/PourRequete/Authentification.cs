﻿namespace AppTirelire
{
    /**********************************************************/
    /* Authentification : classe qui correspond au format Json*/
    /* necessaire a l'authentification sur le serveur         */ 
    /**********************************************************/
    internal class Authentification
    {
        public string email {get; set;}                         // mail de l'utilisateur
        public string auth_token { get; set; }                  // le numero de serie du telephone     

        public Authentification(string email, string auth_token)
        {
            this.email = email;
            this.auth_token = auth_token;
        }
    }
}