﻿using System.Collections.Generic;

namespace AppTirelire
{
    /*************************************************************/
    /* ReponseTirelires : classe qui correspond au format Json   */
    /* de la reponse de la recherche des tirelires              */
    /************************************************************/
    internal class ReponseTirelires
    {
        public List<MoneyBox> records { get; set; }             // liste des tirelires trouvees

        public ReponseTirelires(List<MoneyBox> records)
        {
            this.records = records;
        }
               
    }
}