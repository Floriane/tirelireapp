﻿namespace AppTirelire
{
    /*************************************************************/
    /* ReponseLogin : classe qui correspond au format Json      */
    /* de la reponse suite a une requette de login              */
    /************************************************************/
    internal class ReponseLogin
    {
        public string message { get; set; }                         // message contenu dans le corps de la reponse
        public string jwt { get; set; }                             // jeton genere par le serveur

       public ReponseLogin(string message, string jwt)
        {
            this.message = message;
            this.jwt = jwt;
        }

    }
}