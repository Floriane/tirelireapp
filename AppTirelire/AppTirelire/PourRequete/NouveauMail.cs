﻿namespace AppTirelire
{
    internal class NouveauMail
    {
        public string email { get; set; }
        public string jwt { get; set; }
        public string id_money_box { get; set; }

        public NouveauMail(string email, string jwt, string nomTirelire)
        {
            this.email = email;
            this.jwt = jwt;
            this.id_money_box = nomTirelire;
        }
    }
}