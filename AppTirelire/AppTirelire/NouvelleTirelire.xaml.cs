﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /****************************************************************************************************/
    /* NouvelleTirelire : ecran qui confirme l'ajout d'une nouvelle tirelire au compte de l'utilisateur */
    /****************************************************************************************************/
    public partial class NouvelleTirelire : ContentPage
    {
        private Tirelire nouvelleTirelire;

		public NouvelleTirelire (string id)
		{
			InitializeComponent ();

            nouvelleTirelire = new Tirelire (id);

            info.Text = "Tirelire " + id + " ajoutée";            
		}

        async void ActionAccesTirelire(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HomeTirelire(nouvelleTirelire));                                         // On accede a la page de la tirelire qui vient d'etre ajoutee
        }

        async void ActionRetourTirelire(object sender, EventArgs args)
        {
            await Navigation.PopToRootAsync();                                                      // On retire toutes les pages de la pile de navigation
            await Navigation.PushAsync(new Home());                                                 // On accede a la page Home ou se trouve la liste de toutes les tirelires
        }
    }
}