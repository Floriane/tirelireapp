﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppTirelire
{
    /****************************************************************************************************/
    /* AjouterUtilisateur : ecran qui permet d'ajouter un utilisateur a une tirelire                     */
    /****************************************************************************************************/
    public partial class AjouterUtilisateur : ContentPage
    {
        private string email;
        private string role;
        private Tirelire tirelire;

		public AjouterUtilisateur(Tirelire t)
		{
            InitializeComponent();
            tirelire = t;
            selectionRole.SelectedIndexChanged += (sender, args) =>
            {
                if(selectionRole.SelectedIndex == -1)
                {
                    role = null;
                }
                else
                {
                    role = selectionRole.Items[selectionRole.SelectedIndex];
                }
            };

            info.Text = "Veuillez saisir le mail de la personne à ajouter à la tirelire " + tirelire.ToString() + " ainsi que son rôle.";

        }

        void EntryMailTexteModifie(object sender, TextChangedEventArgs e)
        {
            email = e.NewTextValue;                                     // mise a jour de la valeur du mail saisie
        }

        async void ActionValider(object sender, EventArgs args)
        {
            if (role != null)
            {
                string roleServeur;
                switch (role)
                {
                    case "Ami":
                        roleServeur = "friend";
                        break;
                    case "Enfant":
                        roleServeur = "children";
                        break;
                    case "Parent":
                        roleServeur = "parent";
                        break;
                    default:
                        roleServeur = "withoutRole";
                        break;
                }

                HttpClient client = new HttpClient();
                var uri = new Uri(string.Format("http://192.168.1.47/API_CA/api/waitingUser/create.php"));                                                   // adresse de l'api pour creer un compte
                var json = JsonConvert.SerializeObject(new AjoutUtilisateur(Preferences.Get("jwt", "-1"), email, roleServeur, Convert.ToInt32(tirelire.ToString())));                         // creation du corps de la requete
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);                                                                                // requete HTTP

                if (response.IsSuccessStatusCode)
                {
                    await Navigation.PushAsync(new NouveauUtilisateur(email, tirelire));                                     // si le code est bon on se rend sur la page pour annoncer l'ajout
                }
                else
                {
                    info.Text = "Impossible d'ajouter un utilisateur";
                }
            }
            else
            {
                info.Text = "Veuillez selectionner un rôle.";
            }          
        }

        async void ActionRetour(object sender, EventArgs args)
        {
            await Navigation.PopAsync();                                                     // Retour a la page de la liste des tirelires
        }
    }
}